//
//  ResourceLoadingViewModel.swift
//  EssentailFeed
//
//  Created by MIF50 on 20/01/2022.
//

import Foundation

public struct ResourceLoadingViewModel {
    public let isLoading: Bool
}
