//
//  ResourceLoadingView.swift
//  EssentailFeed
//
//  Created by MIF50 on 20/01/2022.
//

import Foundation

public protocol ResourceLoadingView {
    func display(_ viewModel: ResourceLoadingViewModel)
}
