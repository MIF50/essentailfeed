//
//  ImageCommentEndpoint.swift
//  EssentailFeed
//
//  Created by MIF50 on 15/02/2022.
//

import Foundation

public enum ImageCommentEndpoint {
    case get(UUID)
    
    public func url(baseURL: URL)-> URL {
        switch self {
        case let .get(id):
            return baseURL.appendingPathComponent("/v1/image/\(id)/comments")
        }
    }
}
