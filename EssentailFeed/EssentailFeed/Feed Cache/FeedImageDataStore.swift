//
//  FeedImageDataStore.swift
//  EssentailFeed
//
//  Created by MIF50 on 16/12/2021.
//

import Foundation

public protocol FeedImageDataStore {
    func insert(data: Data, for url: URL) throws
    func retrieve(dataForURL url: URL) throws -> Data?
}
