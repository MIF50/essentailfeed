//
//  FeedImageViewModel.swift
//  EssentailFeed
//
//  Created by MIF50 on 11/12/2021.
//

import Foundation

public struct FeedImageViewModel {
    public let description: String?
    public let location: String?
    
    public var hasLocation: Bool {
        return location != nil
    }
}
