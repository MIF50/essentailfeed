//
//  FeedPresenter.swift
//  EssentailFeed
//
//  Created by MIF50 on 10/12/2021.
//

import Foundation

public final class FeedPresenter {
    
    public static var title: String {
        NSLocalizedString("FEED_VIEW_TITLE",
                          tableName: "Feed",
                          bundle: Bundle(for: FeedPresenter.self),
                          comment: "title for a feed view")
    }
    
    public static func map(_ feed: [FeedImage])-> FeedViewModel {
        FeedViewModel(feed: feed)
    }
}
