//
//  FeedViewModel.swift
//  EssentailFeed
//
//  Created by MIF50 on 10/12/2021.
//

import Foundation

public struct FeedViewModel {
    public let feed: [FeedImage]
}
