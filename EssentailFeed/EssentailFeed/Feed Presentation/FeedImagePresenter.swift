//
//  FeedImagePresenter.swift
//  EssentailFeedTests
//
//  Created by MIF50 on 11/12/2021.
//

import Foundation

public final class FeedImagePresenter {
    
    public static func map( _ image: FeedImage)-> FeedImageViewModel {
        FeedImageViewModel(description: image.description,
                           location: image.location)
    }
}
