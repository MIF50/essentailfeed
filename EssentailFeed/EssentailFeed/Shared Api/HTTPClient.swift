//
//  HTTPClient.swift
//  EssentailFeed
//
//  Created by MIF50 on 25/08/2021.
//

import Foundation

public protocol HTTPClientTask {
    func cancel()
}

public protocol HTTPClient {
    typealias Result = Swift.Result<(Data,HTTPURLResponse),Error>
    
    /// The completion handler can be invoked in any threads.
    /// Clients are responsible to dispatch to appropriate threads, if needed.
    @discardableResult
    func get(from url: URL,completion: @escaping ((Result)-> Void))-> HTTPClientTask
}
