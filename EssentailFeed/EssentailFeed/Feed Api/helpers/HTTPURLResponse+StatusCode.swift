//
//  HTTPURLResponse+StatusCode.swift
//  EssentailFeed
//
//  Created by MIF50 on 14/12/2021.
//

import Foundation

extension HTTPURLResponse {
    
    private static var OK_200: Int { return 200 }
    
    var isOK: Bool {
        return statusCode == HTTPURLResponse.OK_200
    }
    
    var is2xx: Bool {
        return (200...299).contains(statusCode)
    }
}
