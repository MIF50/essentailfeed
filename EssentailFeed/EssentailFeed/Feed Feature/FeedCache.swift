//
//  FeedCache.swift
//  EssentailFeed
//
//  Created by MIF50 on 23/12/2021.
//

import Foundation

public protocol FeedCache {
    func save(_ feed: [FeedImage]) throws
}
