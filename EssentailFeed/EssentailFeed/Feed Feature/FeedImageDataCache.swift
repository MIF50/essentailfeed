//
//  FeedImageDataCache.swift
//  EssentailFeed
//
//  Created by MIF50 on 24/12/2021.
//

import Foundation

public protocol FeedImageDataCache {
    func save(_ data:Data,for url: URL) throws
}
