//
//  FeedImageDataLoader.swift
//  EssentailFeediOS
//
//  Created by MIF50 on 20/10/2021.
//

import Foundation

public protocol FeedImageDataLoader {    
    func loadImageData(from url: URL) throws -> Data
}
