//
//  ListSnapshotTests.swift
//  EssentailFeediOSTests
//
//  Created by MIF50 on 02/02/2022.
//

import XCTest
import EssentailFeediOS
@testable import EssentailFeed

class ListSnapshotTests: XCTestCase {
    
    func test_emptyList() {
        let sut = makeSUT()
        
        sut.display(emptyList())
        
        assert(snapshot: sut.snapshot(for: .iPhone8(style: .light)), name: "EMPTY_FEED_LIGHT")
        assert(snapshot: sut.snapshot(for: .iPhone8(style: .dark)), name: "EMPTY_FEED_DARK")
    }
    
    func test_ListWithErrorMessage() {
        let sut = makeSUT()
        
        sut.display(.error(message: "This is a\nmulti-line\nerror message"))
        
        assert(snapshot: sut.snapshot(for: .iPhone8(style: .light)), name: "LIST_WITH_ERROR_MESSAGE_LIGHT")
        assert(snapshot: sut.snapshot(for: .iPhone8(style: .dark)), name: "LIST_WITH_ERROR_MESSAGE_DARK")
        assert(snapshot: sut.snapshot(for: .iPhone8(style: .dark,contentSize: .extraExtraExtraLarge)), name: "LIST_WITH_ERROR_MESSAGE_DARK_extraExtraExtraLarge")

    }
    
    // MARK: - Helper
    
    private func makeSUT()-> ListViewController {
        let controller = ListViewController()
        controller.loadViewIfNeeded()
        controller.tableView.separatorStyle = .none
        controller.tableView.showsVerticalScrollIndicator = false
        controller.tableView.showsHorizontalScrollIndicator = false
        return controller
    }
    
    private func emptyList()-> [CellController] {
        return []
    }

}
