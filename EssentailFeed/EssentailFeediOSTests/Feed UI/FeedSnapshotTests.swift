//
//  FeedSnapshotTests.swift
//  EssentailFeediOSTests
//
//  Created by MIF50 on 29/12/2021.
//

import XCTest
import EssentailFeediOS
@testable import EssentailFeed

class FeedSnapshotTests: XCTestCase {
    
    func test_feedWithContent() {
        let sut = makeSUT()
        
        sut.display(feedWithContent())
        
        assert(snapshot: sut.snapshot(for: .iPhone8(style: .light)), name: "FEED_WITH_CONTENT_LIGHT")
        assert(snapshot: sut.snapshot(for: .iPhone8(style: .dark)), name: "FEED_WITH_CONTENT_DARK")
        assert(snapshot: sut.snapshot(for: .iPhone8(style: .light,contentSize: .extraExtraExtraLarge)), name: "FEED_WITH_CONTENT_LIGHT_extraExtraExtraLarge")
    }
    
    func test_feedWithFailedImageLoader() {
        let sut = makeSUT()
        
        sut.display(feedWithFailedImageLoader())
        
        assert(snapshot: sut.snapshot(for: .iPhone8(style: .light)), name: "FEED_WITH_FAILED_IMAGE_LOADER_LIGHT")
        assert(snapshot: sut.snapshot(for: .iPhone8(style: .dark)), name: "FEED_WITH_FAILED_IMAGE_LOADER_DARK")
        assert(snapshot: sut.snapshot(for: .iPhone8(style: .dark,contentSize: .extraExtraExtraLarge)), name: "FEED_WITH_FAILED_IMAGE_LOADER_DARK_extraExtraExtraLarge")
    }
    
    func test_feedWithLoadMoreIndicator() {
        let sut = makeSUT()
        
        sut.display(feedWithLoadMoreIndicator())
        
        assert(snapshot: sut.snapshot(for: .iPhone8(style: .light)), name: "FEED_WITH_LOAD_MORE_INDCATOR_LIGHT")
        assert(snapshot: sut.snapshot(for: .iPhone8(style: .dark)), name: "FEED_WITH_LOAD_MORE_INDCATOR_DARK")
    }
    
    func test_feedWithLoadMoreError() {
        let sut = makeSUT()
        
        sut.display(feedWithLoadMoreError())
        
        assert(snapshot: sut.snapshot(for: .iPhone8(style: .light)), name: "FEED_WITH_LOAD_MORE_ERROR_LIGHT")
        assert(snapshot: sut.snapshot(for: .iPhone8(style: .dark)), name: "FEED_WITH_LOAD_MORE_ERROR_DARK")
        assert(snapshot: sut.snapshot(for: .iPhone8(style: .light,contentSize: .extraExtraExtraLarge)), name: "FEED_WITH_LOAD_MORE_ERROR_extraExtraExtraLarge")
    }
    
    // MARK: - Helper
    
    private func makeSUT()-> ListViewController {
        let bundle = Bundle(for: ListViewController.self)
        let storyboard = UIStoryboard(name: "Feed", bundle: bundle)
        let controller = storyboard.instantiateInitialViewController() as! ListViewController
        controller.loadViewIfNeeded()
        controller.tableView.showsVerticalScrollIndicator = false
        controller.tableView.showsHorizontalScrollIndicator = false
        return controller
    }

    
    private func feedWithContent()-> [ImageStub] {
        return [
            ImageStub(
                description: "The East Side Gallery is an open-air gallery in Berlin. It consists of a series of murals painted directly on a 1,316 m long remnant of the Berlin Wall, located near the centre of Berlin, on Mühlenstraße in Friedrichshain-Kreuzberg. The gallery has official status as a Denkmal, or heritage-protected landmark.",
                location: "East Side Gallery\nMemorial in Berlin, Germany",
                image: UIImage.make(withColor: .red)
            ),
            ImageStub(
                description: "Garth Pier is a Grade II listed structure in Bangor, Gwynedd, North Wales.",
                location: "Garth Pier",
                image: UIImage.make(withColor: .green)
            )
        ]
    }
    
    private func feedWithFailedImageLoader()-> [ImageStub] {
        return [
            ImageStub(description: nil, location: "Cannon Street, London", image: nil),
            ImageStub(description: nil, location: "Brighton Seafront", image: nil)
        ]
    }
    
    private func feedWithLoadMoreIndicator() -> [CellController] {        
        let loadMore = LoadMoreCellController(callback: {})
        loadMore.display(ResourceLoadingViewModel(isLoading: true))
        
        return feedWith(loadMore: loadMore)
    }
    
    private func feedWithLoadMoreError() -> [CellController] {
        let loadMore = LoadMoreCellController(callback: {})
        loadMore.display(ResourceErrorViewModel(message: "this a multiline\nmessage error"))
        
        return feedWith(loadMore: loadMore)
    }
    
    private func feedWith(loadMore: LoadMoreCellController) -> [CellController] {
        let stub = feedWithContent().last!
        let cellController = FeedImageCellController(viewModel: stub.viewModel,delegate: stub, selection: {})
        stub.controller = cellController
        return [
            CellController(id: UUID() ,cellController),
            CellController(id: UUID(), loadMore)
        ]
    }
}

private extension ListViewController {
    
    func display(_ stubs: [ImageStub]) {
        let cells: [CellController] = stubs.map { stub  in
            let cellController = FeedImageCellController(viewModel: stub.viewModel,delegate: stub, selection: {})
            stub.controller = cellController
            return CellController(id: UUID() ,cellController)
        }
        
        display(cells)
    }
}

private class ImageStub: FeedImageCellControllerDelegate {
    
    private let image: UIImage?
    let viewModel: FeedImageViewModel
    weak var controller: FeedImageCellController?
    
    init(description: String?,location: String?,image: UIImage?) {
        self.viewModel = FeedImageViewModel(description: description,
                                            location: location)
        self.image = image
    }
    
    func didRequestImage() {
        controller?.display(ResourceLoadingViewModel(isLoading: false))
        
        if let image = image {
            controller?.display(image)
            controller?.display(ResourceErrorViewModel(message: .none))
        } else {
            controller?.display(ResourceErrorViewModel(message: "any"))
        }
    }
    
    func didCancelImageRequest() {}
}
