//
//  ImageCommentSnapshotTests.swift
//  EssentailFeediOSTests
//
//  Created by MIF50 on 03/02/2022.
//

import XCTest
import EssentailFeediOS
@testable import EssentailFeed

class ImageCommentSnapshotTests: XCTestCase {
    
    func test_imageCommentWithContent() {
        let sut = makeSUT()
        
        sut.display(comments())
        
        assert(snapshot: sut.snapshot(for: .iPhone8(style: .light)), name: "IMAGE_COMMENTS_CONTENT_LIGHT")
        assert(snapshot: sut.snapshot(for: .iPhone8(style: .dark)), name: "IMAGE_COMMENTS_CONTENT_DARK")
        assert(snapshot: sut.snapshot(for: .iPhone8(style: .light,contentSize: .extraExtraExtraLarge)), name: "IMAGE_COMMENTS_CONTENT_LIGHT_extraExtraExtraLarge")
    }
    
    // MARK: - Helper
    
    private func makeSUT()-> ListViewController {
        let bundle = Bundle(for: ListViewController.self)
        let storyboard = UIStoryboard(name: "ImageComments", bundle: bundle)
        let controller = storyboard.instantiateInitialViewController() as! ListViewController
        controller.loadViewIfNeeded()
        controller.tableView.showsVerticalScrollIndicator = false
        controller.tableView.showsHorizontalScrollIndicator = false
        return controller
    }
    
    private func comments()-> [CellController] {
        commentsController().map { CellController(id: UUID(),$0) }
    }
    
    private func commentsController()-> [ImageCommentCellController] {
        return [
            ImageCommentCellController(model: ImageCommentViewModel(
                message: "The East Side Gallery is an open-air gallery in Berlin. It consists of a series of murals painted directly on a 1,316 m long remnant of the Berlin Wall, located near the centre of Berlin, on Mühlenstraße in Friedrichshain-Kreuzberg. The gallery has official status as a Denkmal, or heritage-protected landmark.",
                date: "10 years ago",
                username: "a long long long long username")),
            ImageCommentCellController(model: ImageCommentViewModel(
                message: "Garth Pier is a Grade II listed structure in Bangor, Gwynedd, North Wales.",
                date: "1 day ago",
                username: "a username")),
            ImageCommentCellController(model: ImageCommentViewModel(
                message: "nice",
                date: "1 minute ago",
                username: "a."))
        ]
    }
}
