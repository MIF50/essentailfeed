//
//  FeedLocalizationTests.swift
//  EssentailFeediOSTests
//
//  Created by MIF50 on 12/11/2021.
//

import XCTest
import EssentailFeed

class FeedLocalizationTests: XCTestCase {
    
    func test_localizedStrings_haveKeyAndValueForAllSupportedLocalications() {
        let table = "Feed"
        let bundle = Bundle(for: FeedPresenter.self)
        assertLocalizedKeyAndValuesExist(in: bundle, table)
    }
}
