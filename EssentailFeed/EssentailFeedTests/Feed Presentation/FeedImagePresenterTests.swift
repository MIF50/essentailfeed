//
//  FeedImagePresenterTests.swift
//  EssentailFeedTests
//
//  Created by MIF50 on 11/12/2021.
//

import XCTest
import EssentailFeed

class FeedImagePresenterTests: XCTestCase {

    func test_map_createsViewModel() {
        let image = uniqueImage()
        
        let viewModel = FeedImagePresenter.map(image)
        
        XCTAssertEqual(viewModel.description, image.description)
        XCTAssertEqual(viewModel.location, image.location)
    }
}
