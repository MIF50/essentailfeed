//
//  ImageCommentEndpointTests.swift
//  EssentailFeedTests
//
//  Created by MIF50 on 15/02/2022.
//

import XCTest
import EssentailFeed

class ImageCommentEndpointTests: XCTestCase {
    
    func test_imageComments_endpointURL() {
        let imageUUID = UUID(uuidString: "2AB2AE66-A4B7-4A16-B374-51BBAC8DB086")!
        let baseURL = URL(string: "http://base-url.com")!
        
        let recieved = ImageCommentEndpoint.get(imageUUID).url(baseURL: baseURL)
        let expected = URL(string: "http://base-url.com/v1/image/2AB2AE66-A4B7-4A16-B374-51BBAC8DB086/comments")!

        XCTAssertEqual(recieved, expected)
    }
}
