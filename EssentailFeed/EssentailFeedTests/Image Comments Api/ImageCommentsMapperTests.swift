//
//  LoadImageCommentsFromRemoteUseCaseTests.swift
//  EssentailFeedTests
//
//  Created by MIF50 on 08/01/2022.
//

import XCTest
import EssentailFeed

class ImageCommentsMapperTests: XCTestCase {
    
    func test_map_throwsErrorOnNon2xxHttpResponse() throws {
        let json = makeItemJSON([])
        let samples = [199,150,300,400,500]
        
        try samples.forEach { code in
            XCTAssertThrowsError(
                try ImageCommentsMapper.map(json, from: HTTPURLResponse(statusCode: code))
            )
        }
    }
    
    func test_map_throwsErrorOn2xxHttpResponseWithInvalidJSON() throws {
        let invalidJson = Data("invalidJson".utf8)
        let samples = [200,201,250,280,299]
        
        try samples.forEach { code in
            XCTAssertThrowsError(
                try ImageCommentsMapper.map(invalidJson, from: HTTPURLResponse(statusCode: code))
            )
        }
    }
    
    func test_map_deliversNoItemsOn2xxHttpResponseWithEmptyJsonList() throws {
        let emptyJsonList = makeItemJSON([])
        let samples = [200,201,250,280,299]
        
        try samples.forEach { code in
            let result = try ImageCommentsMapper.map(emptyJsonList, from: HTTPURLResponse(statusCode: code))
            XCTAssertEqual(result, [])
        }
    }
    
    func test_map_deliversItemsOn2xxHttpResponseWithJsonItems() throws {
        let item1 = makeItem(id: UUID(),
                             message: "a message",
                             createdAt: (Date(timeIntervalSince1970: 1598627222), "2020-08-28T15:07:02+00:00"),
                             username: "a username")
        
        let item2 = makeItem(id: UUID(),
                             message: "anthor message",
                             createdAt: (Date(timeIntervalSince1970: 1577881882), "2020-01-01T12:31:22+00:00"),
                             username: "anther username")
    
        let json = makeItemJSON([item1.json,item2.json])

        let samples = [200,201,250,280,299]
        try samples.forEach { code in
            let result = try ImageCommentsMapper.map(json, from: HTTPURLResponse(statusCode: code))
            XCTAssertEqual(result, [item1.model,item2.model])
        }
    }
    
    // MARK:- Helper
    
    private func makeItem(id: UUID,
                          message: String,
                          createdAt: (date: Date,ios8601String: String),
                          username: String)-> (model: ImageComment, json: [String: Any]) {
        let item = ImageComment(id: id,
                                message: message,
                                createdAt: createdAt.date,
                                username: username)
        let json:[String: Any] = [
            "id": id.uuidString,
            "message": message,
            "created_at": createdAt.ios8601String,
            "author": [
                "username": username
            ]
        ]
        
        return (item,json)
    }
}
