//
//  SharedLocalizationTests.swift
//  EssentailFeedTests
//
//  Created by MIF50 on 16/01/2022.
//

import XCTest
import EssentailFeed

class SharedLocalicationTests: XCTestCase {

    func test_localizedStrings_haveKeyAndValueForAllSupportedLocalications() {
        let table = "Shared"
        let bundle = Bundle(for: LoadResourcePresenter<Any,DummyView>.self)
        assertLocalizedKeyAndValuesExist(in: bundle, table)
    }
    
    private class DummyView: ResourceView {
        func display(_ viewModel: Any) { }
    }
}
