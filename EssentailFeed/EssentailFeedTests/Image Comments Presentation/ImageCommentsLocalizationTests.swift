//
//  ImageCommentsLocalizationTests.swift
//  EssentailFeedTests
//
//  Created by MIF50 on 27/01/2022.
//

import XCTest
import EssentailFeed

class ImageCommentsLocalizationTests: XCTestCase {
    
    func test_localizedStrings_haveKeyAndValueForAllSupportedLocalications() {
        let table = "ImageComments"
        let bundle = Bundle(for: ImageCommentsPresenter.self)
        assertLocalizedKeyAndValuesExist(in: bundle, table)
    }
}
