//
//  SharedTestHelper.swift
//  EssentailFeedTests
//
//  Created by MIF50 on 28/09/2021.
//

import Foundation


func anyURL()-> URL {
    return URL(string: "http://any-url.com")!
}

func anyNSError()-> NSError {
    NSError(domain: "any Error", code: 0)
}

func anyData()-> Data {
    Data("any Data".utf8)
}

func makeItemJSON(_ items: [[String: Any]]) -> Data {
    let itemsJSON = ["items": items]
    return try! JSONSerialization.data(withJSONObject: itemsJSON)
}
extension HTTPURLResponse {
    convenience init(statusCode: Int) {
        self.init(url: anyURL(), statusCode: statusCode, httpVersion: nil, headerFields: nil)!
    }
}

extension Date {
    
    func adding(seconds: TimeInterval)-> Date {
        return self + seconds
    }
    
    func adding(days: Int,calendar: Calendar = Calendar(identifier: .gregorian))-> Date {
        return calendar.date(byAdding: .day, value: days, to: self)!
    }
    
    func adding(minutes: Int,calendar: Calendar = Calendar(identifier: .gregorian))-> Date {
        return calendar.date(byAdding: .minute, value: minutes, to: self)!
    }
}
