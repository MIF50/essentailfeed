//
//  LoadFeedFromCachedUserCaseTests.swift
//  EssentailFeedTests
//
//  Created by MIF50 on 25/09/2021.
//

import XCTest
import EssentailFeed

class LoadFeedFromCachedUseCaseTests: XCTestCase {
    
    func test_init_doesNotMessageStoreUponCreateion() {
        let (_,store) = makeSUT()
        
        XCTAssertTrue(store.receivedMessages.isEmpty)
    }
    
    func test_load_requestCacheRetreival() {
        let (sut,store) = makeSUT()
        
        _ = try? sut.load()
        
        XCTAssertEqual(store.receivedMessages, [.retrieval])
    }
    
    func test_load_failsOnRetrievalError() {
        let (sut,store) = makeSUT()
        let retrivelError = anyNSError()
        
        expect(sut, toCompleteWith: .failure(retrivelError), when: {
            store.completeRetrieval(with: retrivelError)
        })
    }
    
    func test_load_deliversNoImageOnEmptyCache() {
        let (sut,store) = makeSUT()
        
        expect(sut, toCompleteWith: .success([]), when: {
            store.completeRetrievalWithEmptyCache()
        })
    }
    
    func test_load_deliversCachedImagesOnNonExpireCache() {
        let feed = uniqueImageFeed()
        let fixedCurrentDate = Date()
        let nonExpiredTimestamp = fixedCurrentDate.minusFeedCacheMaxAge().adding(seconds: 1)
        let (sut,store) = makeSUT(currentData: { fixedCurrentDate })
        
        expect(sut, toCompleteWith: .success(feed.models), when: {
            store.completeRetrieval(with: feed.local,timestamp: nonExpiredTimestamp)
        })
    }
    
    func test_load_deliversNoImagesOnCacheExpiration() {
        let feed = uniqueImageFeed()
        let fixedCurrentDate = Date()
        let expirationTimestamp = fixedCurrentDate.minusFeedCacheMaxAge()
        let (sut,store) = makeSUT(currentData: { fixedCurrentDate })
        
        expect(sut, toCompleteWith: .success([]), when: {
            store.completeRetrieval(with: feed.local,timestamp: expirationTimestamp)
        })
    }
    
    func test_load_deliversNoImagesOnExpiredCache() {
       let feed = uniqueImageFeed()
       let fixedCurrentDate = Date()
       let expiredTimestamp = fixedCurrentDate.minusFeedCacheMaxAge().adding(seconds: -1)
       let (sut, store) = makeSUT(currentData: { fixedCurrentDate })
       
       expect(sut, toCompleteWith: .success([]), when: {
           store.completeRetrieval(with: feed.local,timestamp: expiredTimestamp)
       })
    }
    
    func test_load_hasNoSideEffectOnRetrievalError() {
        let (sut,store) = makeSUT()
        store.completeRetrieval(with: anyNSError())

        _ = try? sut.load()
        
        XCTAssertEqual(store.receivedMessages, [.retrieval])
    }
    
    func test_load_hasNoSideEffectOnEmptyCache() {
        let (sut,store) = makeSUT()
        store.completeRetrievalWithEmptyCache()

        _ = try? sut.load()
        
        XCTAssertEqual(store.receivedMessages, [.retrieval])
    }
    
    func test_load_hasNoSideEffectOnNonExpiredCache() {
        let feed = uniqueImageFeed()
        let fixedCurrenDate = Date()
        let nonExpiredTimestamp = fixedCurrenDate.minusFeedCacheMaxAge().adding(seconds: 1)
        let (sut,store) = makeSUT(currentData: { fixedCurrenDate })
        store.completeRetrieval(with: feed.local, timestamp: nonExpiredTimestamp)

        _ = try? sut.load()

        XCTAssertEqual(store.receivedMessages, [.retrieval])
    }
    
    func test_load_hasNoSideEffectsOnExpirationCache() {
        let feed = uniqueImageFeed()
        let fixedCurrentDate = Date()
        let expiredTimestamp = fixedCurrentDate.minusFeedCacheMaxAge()
        let (sut,store) = makeSUT(currentData: { fixedCurrentDate })
        store.completeRetrieval(with: feed.local,timestamp: expiredTimestamp)

        _ = try? sut.load()
        
        XCTAssertEqual(store.receivedMessages, [.retrieval])
    }
    
    func test_load_hasNoSideEffectOnExpiredCache() {
        let feed = uniqueImageFeed()
        let fixedCurrentDate = Date()
        let expiredTimestamp = fixedCurrentDate.minusFeedCacheMaxAge().adding(seconds: -1)
        let (sut,store) = makeSUT(currentData: { fixedCurrentDate })
        store.completeRetrieval(with: feed.local,timestamp: expiredTimestamp)

        _ = try? sut.load()
    
        XCTAssertEqual(store.receivedMessages, [.retrieval])
    }
    
    // MARK: - Helper
    private func makeSUT(currentData:@escaping (()-> Date) = Date.init,
                         file: StaticString = #filePath,
                         line: UInt = #line)-> (sut: LocalFeedLoader,store: FeedStoreSpy) {
        let store = FeedStoreSpy()
        let sut = LocalFeedLoader(store: store,currentDate: currentData)
        trackForMemoryLeaks(store,file: file,line: line)
        trackForMemoryLeaks(sut,file: file,line: line)
        return (sut,store)
    }
    
    private func expect(
        _ sut: LocalFeedLoader,
        toCompleteWith expectedResult:Swift.Result<[FeedImage],Error>,
        when action: (()-> Void),
        file: StaticString = #filePath,
        line: UInt = #line
    ) {
        action()
        
        let receivedResult = Result { try sut.load() }
        switch (receivedResult,expectedResult) {
        case let ((.success(receivedImages)),(.success(expectedImages))):
            XCTAssertEqual(receivedImages, expectedImages,file: file,line: line)
            
        case let ((.failure(receivedError as NSError)),(.failure(expectedError as NSError))):
            XCTAssertEqual(receivedError, expectedError,file: file,line: line)
        default:
            XCTFail("Expect result \(expectedResult) ,but got \(receivedResult) instead")
        }
    }
}

