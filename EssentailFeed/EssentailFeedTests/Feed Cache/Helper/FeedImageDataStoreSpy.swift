//
//  FeedImageDataStoreSpy.swift
//  EssentailFeedTests
//
//  Created by MIF50 on 16/12/2021.
//

import Foundation
import EssentailFeed

final class FeedImageDataStoreSpy: FeedImageDataStore {
   
    enum Message: Equatable {
        case save(data: Data,for: URL)
        case retrieve(dataFor: URL)
    }
    
    private(set) var receivedMessages = [Message]()
    private var retrievalResult: Result<Data?,Error>?
    private var insertionResult: Result<Void,Error>?
    
    func insert(data: Data, for url: URL) throws {
        receivedMessages.append(.save(data: data, for: url))
        try insertionResult?.get()
    }
    
    func retrieve(dataForURL url: URL) throws -> Data? {
        receivedMessages.append(.retrieve(dataFor: url))
        return try retrievalResult?.get()
    }
    
    func completeInsertion(with error: Error,at index: Int = 0) {
        insertionResult = .failure(error)
    }
    
    func completeInsertionSuccessfully(at index: Int = 0) {
        insertionResult = .success(())
    }
    
    func completeRetrieval(with error: Error,at index: Int = 0) {
        retrievalResult = .failure(error)
    }
    
    func completeRetrieval(with data: Data? , at index: Int = 0) {
        retrievalResult = .success(data)
    }
}
