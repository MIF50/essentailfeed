//
//  FeedCacheTestHelper.swift
//  EssentailFeedTests
//
//  Created by MIF50 on 28/09/2021.
//

import Foundation
import EssentailFeed

func uniqueImage()-> FeedImage {
    FeedImage(id: UUID(), description: "any", location: "any", url: anyURL())
}

func uniqueImageFeed()->(models: [FeedImage],local: [LocalFeedImage]) {
    let models = [uniqueImage(),uniqueImage()]
    let local = models.map({
        LocalFeedImage(id: $0.id,
                       description: $0.description,
                       location: $0.location,
                       url: $0.url)
    })
    return(models,local)
}

extension Date {
    
    var feedCacheMaxAgeInDays: Int {
        return 7
    }
    
    func minusFeedCacheMaxAge()-> Date {
        adding(days: -feedCacheMaxAgeInDays)
    }
}
