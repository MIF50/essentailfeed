//
//  CoreDataFeedImageDataStoreTests.swift
//  EssentailFeedTests
//
//  Created by MIF50 on 17/12/2021.
//

import XCTest
import EssentailFeed

class CoreDataFeedImageDataStoreTests: XCTestCase {
    
    func test_retrieveImageData_deliversNotFoundWhenEmpty() {
        let sut = makeSUT()
        
        expect(sut, toCompleteRetrievalWith: notFound(), for: anyURL())
    }
    
    func test_retrieveImageData_deliversNotFoundWhenStoredDataURLDoesNotMatch() {
        let sut = makeSUT()
        let url = URL(string: "https://a-url.com")!
        let notMatchingURL = URL(string: "https://anthor-url.com")!
        
        insert(anyData(), for: url, into: sut)
        
        expect(sut, toCompleteRetrievalWith: notFound(), for: notMatchingURL)
    }
    
    func test_retrieveImageData_deliversFoundDataWhenThereIsAStoreImageDataMatchingURL() {
        let sut = makeSUT()
        let storeData = anyData()
        let matchingURL = URL(string: "https://a-url.com")!
        
        insert(storeData, for: matchingURL, into: sut)
        
        expect(sut, toCompleteRetrievalWith: found(storeData), for: matchingURL)
    }
    
    func test_retrieveImageData_deliversLastInsertedValue() {
        let sut = makeSUT()
        let firstStoredData = Data("first".utf8)
        let lastStoredData = Data("last".utf8)
        let url = URL(string: "https://a-url.com")!
        
        insert(firstStoredData, for: url, into: sut)
        insert(lastStoredData, for: url, into: sut)
        
        expect(sut, toCompleteRetrievalWith: found(lastStoredData), for: url)
    }
    
    // MARK: - Helper
    
    private func makeSUT(file: StaticString = #file,
                         line: UInt = #line)-> CoreDataFeedStore {
        let storeURL = URL(fileURLWithPath: "/dev/null")
        let sut = try!  CoreDataFeedStore(storeURL: storeURL)
        trackForMemoryLeaks(sut,file: file,line: line)
        return sut
    }
    
    private func notFound() -> Result<Data?,Error> {
        return .success(.none)
    }
    
    private func found(_ data: Data) -> Result<Data?,Error> {
        return .success(data)
    }
    
    private func localImage(url: URL)-> LocalFeedImage {
        LocalFeedImage(id: UUID(), description: "any description", location: "any localion", url: url)
    }
    
    private func expect(
        _ sut: CoreDataFeedStore,
        toCompleteRetrievalWith expectedResult: Result<Data?,Error>,
        for url: URL,
        file: StaticString = #file,
        line: UInt = #line
    ) {
        let receivedResult = Result { try sut.retrieve(dataForURL: url) }
        switch (receivedResult,expectedResult) {
        case let (.success(receivedData),.success(expectedData)):
            XCTAssertEqual(receivedData, expectedData,file: file,line: line)
        default:
            XCTFail("Exptected result \(expectedResult) but got \(receivedResult) instead.")
        }
    }
    
    private func insert(
        _ data: Data,
        for url: URL,
        into sut: CoreDataFeedStore,
        file: StaticString = #file,
        line: UInt = #line
    ) {
        do {
            let image = localImage(url: url)
            try sut.insert([image], timestamp: Date())
            try sut.insert(data: data, for: url)
        } catch {
            XCTFail("Failed to save \(data) with error \(error)",file: file,line: line)
        }
    }
}
