//
//  CachedFeedImageDataUseCaseTests.swift
//  EssentailFeedTests
//
//  Created by MIF50 on 16/12/2021.
//

import XCTest
import EssentailFeed

class CacheFeedImageDataUseCaseTests: XCTestCase {
    
    func test_init_doesNotMessageStoreUponCreation() {
        let (_, store) = makeSUT()
        
        XCTAssertTrue(store.receivedMessages.isEmpty)
    }
    
    func test_saveImageDataForURL_requestsImageDataInsertionForURL() {
        let (sut, store) = makeSUT()
        let url = anyURL()
        let data = anyData()
        
        try? sut.save(data,for: url)
        
        XCTAssertEqual(store.receivedMessages, [.save(data: data,for: url)])
    }
    
    func test_saveImageDataForURL_failsOnStoreInsertionError() {
        let (sut, store) = makeSUT()
        
        expect(sut, toCompleteWith: failed(), when: {
            let insertionError = anyNSError()
            store.completeInsertion(with: insertionError)
        })
    }
    
    func test_saveImageDataForURL_succeedsOnSuccessfulStoreInsertion() {
        let (sut, store) = makeSUT()
        
        expect(sut, toCompleteWith: .success(()), when: {
            store.completeInsertionSuccessfully()
        })
    }
    
    // MARK: - Helper
    
    private func makeSUT(currentData: @escaping (()-> Date) = Date.init,
                         file: StaticString = #file,
                         line: UInt = #line)->(sut: LocalFeedImageDataLoader,store: FeedImageDataStoreSpy) {
        let store = FeedImageDataStoreSpy()
        let sut = LocalFeedImageDataLoader(store: store)
        
        trackForMemoryLeaks(store)
        trackForMemoryLeaks(sut)
        
        return (sut,store)
    }
    
    private func failed() -> Result<Void,Error> {
        return .failure(LocalFeedImageDataLoader.SaveError.failed)
    }
    
    private func expect(_ sut: LocalFeedImageDataLoader,
                        toCompleteWith expectedResult: Result<Void,Error>,
                        when action: (()-> Void),
                        file: StaticString = #file,
                        line: UInt = #line) {
        action()
        
        let receivedResult = Result { try sut.save(anyData(), for: anyURL()) }
        
        switch (receivedResult,expectedResult) {
        case (.success,.success):
            break
        case let (.failure(receivedError as LocalFeedImageDataLoader.SaveError?),
                  .failure(expectedError as LocalFeedImageDataLoader.SaveError?)):
            XCTAssertEqual(receivedError,expectedError,file: file,line: line)
        default:
            XCTFail("Expecte result \(expectedResult) but got \(receivedResult) instead.",file: file,line: line)
        }
    }
}
