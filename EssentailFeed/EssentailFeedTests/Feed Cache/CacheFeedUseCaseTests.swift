//
//  CacheFeedUseCaseTests.swift
//  EssentailFeedTests
//
//  Created by MIF50 on 20/09/2021.
//

import XCTest
import EssentailFeed

class CacheFeedUseCaseTests: XCTestCase {
    
    func test_init_doesNotMessageStoreUponCreation() {
        let (_,store) = makeSUT()
        
        XCTAssertEqual(store.receivedMessages, [])
    }
    
    func test_save_doesNotRequestCacheInsertionOnDeletionError() {
        let deletionError = anyNSError()
        let (sut,store) = makeSUT()
        store.completeDeletion(with: deletionError)

        try? sut.save(uniqueImageFeed().models)
        
        XCTAssertEqual(store.receivedMessages, [.deleteCachedFeed])
    }
    
    func test_save_requestsNewCachInsertWithTimestampOnSuccessfullDeletion() {
        let timestamp = Date()
        let feed = uniqueImageFeed()
        let (sut,store) = makeSUT(currentData: {timestamp})
        store.completeDeletionSuccessfull()

        try? sut.save(feed.models)
        
        XCTAssertEqual(store.receivedMessages, [.deleteCachedFeed,.insert(feed.local, timestamp)])
    }
    
    func test_save_failsOnDeletionError() {
        let deletionError = anyNSError()
        let (sut,store) = makeSUT()
        
        expect(sut, completeWithError: deletionError, when: {
            store.completeDeletion(with: deletionError)
        })
    }
    
    func test_save_failsOnInsertionError() {
        let insertionError = anyNSError()
        let (sut,store) = makeSUT()
        
        expect(sut, completeWithError: insertionError, when: {
            store.completeDeletionSuccessfull()
            store.completeInsertion(with: insertionError)
        })
    }
    
    func test_save_succeedsOnSuccessfulCacheInsertion() {
        let (sut,store) = makeSUT()
        
        expect(sut, completeWithError: nil, when: {
            store.completeDeletionSuccessfull()
            store.completeInsertionSuccessfull()
        })
    }
    
    // MARK: - Helper
    private func makeSUT(
        currentData:@escaping  (()-> Date) = Date.init,
        file: StaticString = #filePath,
        line: UInt = #line
    )-> (sut: LocalFeedLoader,store: FeedStoreSpy) {
        let store = FeedStoreSpy()
        let sut = LocalFeedLoader(store: store,currentDate: currentData)
        trackForMemoryLeaks(store,file: file,line: line)
        trackForMemoryLeaks(sut,file: file,line: line)
        return (sut,store)
    }
    
    private func expect(
        _ sut: LocalFeedLoader,
        completeWithError expectedError: NSError?,
        when action:(()-> Void),
        file: StaticString = #filePath,
        line: UInt = #line
    ) {
        action()
        do {
            try sut.save(uniqueImageFeed().models)
        } catch {
            XCTAssertEqual(error as NSError?, expectedError,file: file,line: line)
        }
    }
}
