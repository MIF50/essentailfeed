//
//  URLSessionHTTPClientTests.swift
//  EssentailFeedTests
//
//  Created by MIF50 on 31/08/2021.
//

import XCTest
import EssentailFeed

class URLSessionHTTPClientTests: XCTestCase {
    
    override func tearDown() {
        super.tearDown()
        URLProtocolStub.removeStub()
    }
    
    func test_getFromURL_performsGETRequestWithURL() {
        let url = anyURL()
        let exp = expectation(description: "waiting for completions")
        
        URLProtocolStub.observeRequests { request  in
            XCTAssertEqual(request.url, url)
            XCTAssertEqual(request.httpMethod, "GET")
            exp.fulfill()
        }
        
        makeSUT().get(from: url) { _ in }
        wait(for: [exp], timeout: 1.0)
    }
    
    func test_cancelGetFromURLTask_cancelsURLRequest() {
        let receivedError = resultErrorFor(taskHandler: { $0.cancel() }) as NSError?
        XCTAssertEqual(receivedError?.code, URLError.cancelled.rawValue)
    }
    
    func test_getFromURL_failsOnRequestError() {
        let requestError = anyNSError()
        
        let receivedError = resultErrorFor((data: nil, response: nil , error: requestError)) as NSError?

        XCTAssertEqual(receivedError?.code, requestError.code)
        XCTAssertEqual(receivedError?.domain, requestError.domain)
    }
    
    func test_getFromURL_failsOnAllInvalidRePresentationCases() {
        XCTAssertNotNil(resultErrorFor((data: nil,response: nil,error: nil)))
        XCTAssertNotNil(resultErrorFor((data: nil,response: nonHTTPURLResponse(),error: nil)))
        XCTAssertNotNil(resultErrorFor((data: anyData(),response: nil,error: nil)))
        XCTAssertNotNil(resultErrorFor((data: anyData(),response: nil,error: anyNSError())))
        XCTAssertNotNil(resultErrorFor((data: nil,response: nonHTTPURLResponse(),error: anyNSError())))
        XCTAssertNotNil(resultErrorFor((data: nil,response: anyHTTPURLResponse(),error: anyNSError())))
        XCTAssertNotNil(resultErrorFor((data: anyData(),response: nonHTTPURLResponse(),error: anyNSError())))
        XCTAssertNotNil(resultErrorFor((data: anyData(),response: anyHTTPURLResponse(),error: anyNSError())))
        XCTAssertNotNil(resultErrorFor((data: anyData(),response: nonHTTPURLResponse(),error: nil)))
    }
    
    func test_getFromURL_suceedsOnHTTPURLResponseWithData() {
        let data = anyData()
        let response = anyHTTPURLResponse()
        
        let receivedValue = resultValueFor((data: data, response: response, error: nil))
        
        XCTAssertEqual(receivedValue?.data, data)
        XCTAssertEqual(receivedValue?.response.url, response?.url)
        XCTAssertEqual(receivedValue?.response.statusCode, response?.statusCode)
    }
    
    func test_getFromURL_suceedsWithEmptyDataOnHTTPURLResponseWithNilData() {
        let response = anyHTTPURLResponse()
        let emptyData = Data()
        
        let receivedValue = resultValueFor((data: nil, response: response, error: nil))
        
        XCTAssertEqual(receivedValue?.data, emptyData)
        XCTAssertEqual(receivedValue?.response.url, response?.url)
        XCTAssertEqual(receivedValue?.response.statusCode, response?.statusCode)
    }
    
    // MARK:- Helper
    
    private func makeSUT(file: StaticString = #filePath,
                         line: UInt = #line)-> HTTPClient {
        let configuration = URLSessionConfiguration.ephemeral
        configuration.protocolClasses = [URLProtocolStub.self]
        let session = URLSession(configuration: configuration)
        let sut =  URLSessionHTTPClient(session: session)
        trackForMemoryLeaks(sut,file: file,line: line)
        return sut
    }
    
    private typealias TaskHander = ((HTTPClientTask)-> Void)
    
    private typealias Values = ((data: Data?,
                                 response: URLResponse?,
                                 error: Error?))
    
    private func resultErrorFor(_ values: Values? = nil,
                                taskHandler: TaskHander = { _ in },
                                file: StaticString = #filePath,
                                line: UInt = #line)-> Error? {
        let result = resultFor(values,taskHandler: taskHandler,file: file,line:  line)
        
        switch result {
        case let .failure(error):
            return error
        default:
            XCTFail("Expected error but, got \(result) instead",file: file,line: line)
            return nil
        }
    }
    
    private func resultValueFor(_ values: Values?,
                                file: StaticString = #filePath,
                                line: UInt = #line)-> (data: Data,response: HTTPURLResponse)? {
        let result = resultFor(values,file: file,line: line)
        
        switch result {
        case let .success((data,response)):
            return (data,response)
        default:
            XCTFail("Expected success but, got \(result) instead",file: file,line: line)
            return nil
        }
    }
    
    private func resultFor(_ values: Values? = nil,
                           taskHandler: TaskHander = { _ in },
                           file: StaticString = #filePath,
                           line: UInt = #line)-> HTTPClient.Result {
        values.map { URLProtocolStub.stub(data: $0, response: $1, error: $2 as NSError?) }
        let sut = makeSUT(file: file,line: line)
        let exp = expectation(description: "waiting for request")
        
        var recievedResult: HTTPClient.Result!
        taskHandler(sut.get(from: anyURL()) { result in
            recievedResult = result
            exp.fulfill()
        })
        
        wait(for: [exp], timeout: 1.0)
        
        return recievedResult
    }
    
    private func nonHTTPURLResponse()-> URLResponse {
        URLResponse(url: anyURL(), mimeType: nil, expectedContentLength: 0, textEncodingName: nil)
    }
    
    private func anyHTTPURLResponse()-> HTTPURLResponse? {
        HTTPURLResponse(url: anyURL(), statusCode: 200, httpVersion: nil, headerFields: nil)
    }
}
