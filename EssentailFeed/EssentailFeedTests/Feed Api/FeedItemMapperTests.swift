//
//  RemoteFeedLoaderTest.swift
//  EssentailFeedTests
//
//  Created by MIF50 on 16/08/2021.
//

import XCTest
import EssentailFeed

class FeedItemMapperTests: XCTestCase {
    
    func test_map_throwsErrorOnNon200HttpResponse() throws {
        let json = makeItemJSON([])
        let samples = [199,201,300,400,500]
        
        try samples.forEach { code in
            XCTAssertThrowsError(
                try FeedItemsMapper.map(json, from: HTTPURLResponse(statusCode: code))
            )
        }
    }
    
    func test_map_throwsErrorOn200HttpResponseWithInvalidJSON() throws {
        let invalidJson = Data("invalidJson".utf8)
        
        XCTAssertThrowsError(
            try FeedItemsMapper.map(invalidJson, from: HTTPURLResponse(statusCode: 200))
        )
    }
    
    func test_map_deliversNoItemsOn200HttpResponseWithEmptyJsonList() throws {
        let emptyJsonList = makeItemJSON([])
        
        let result = try FeedItemsMapper.map(emptyJsonList, from: HTTPURLResponse(statusCode: 200))
        XCTAssertEqual(result, [])
    }
    
    func test_map_deliversItemsOn200HttpResponseWithJsonItems() throws {
        
        let item1 = makeItem(id: UUID(),
                             imageURL: URL(string: "http://a-url.com")!)
        
        let item2 = makeItem(id: UUID(),
                             description: "a description",
                             location: "a location",
                             imageURL: URL(string: "http://anther-url.com")!)
            
        let json = makeItemJSON([item1.json,item2.json])
        let result = try FeedItemsMapper.map(json, from: HTTPURLResponse(statusCode: 200))
        
        XCTAssertEqual(result, [item1.model,item2.model])
    }
    
    // MARK:- Helper
    
    private func makeItem(id: UUID,
                          description: String? = nil,
                          location: String? = nil,
                          imageURL: URL)-> (model: FeedImage, json: [String: Any]) {
        let item = FeedImage(id: id,
                            description: description,
                            location: location,
                            url: imageURL)
        let json = [
            "id": id.uuidString,
            "description": description,
            "location": location,
            "image": imageURL.absoluteString
        ].compactMapValues{ $0 }
        
        return (item,json)
    }
}
