//
//  FeedEndpointTests.swift
//  EssentailFeedTests
//
//  Created by MIF50 on 15/02/2022.
//

import XCTest
import EssentailFeed

class FeedEndpointTests: XCTestCase {
    
    func test_feedImage_endpointURL() {
        let baseURL = URL(string: "http://base-url.com")!
        
        let recieved = FeedEndpoint.get().url(baseURL: baseURL)

        XCTAssertEqual(recieved.scheme, "http","scheme")
        XCTAssertEqual(recieved.host, "base-url.com","host")
        XCTAssertEqual(recieved.path, "/v1/feed","path")
        XCTAssertEqual(recieved.query, "limit=10","query")
    }
    
    func test_feedImage_endpointURLAfterGivenImage() {
        let image = uniqueImage()
        let baseURL = URL(string: "http://base-url.com")!
        
        let recieved = FeedEndpoint.get(after: image).url(baseURL: baseURL)

        XCTAssertEqual(recieved.scheme, "http","scheme")
        XCTAssertEqual(recieved.host, "base-url.com","host")
        XCTAssertEqual(recieved.path, "/v1/feed","path")
        XCTAssertEqual(recieved.query?.contains("limit=10"),true,"limit query param")
        XCTAssertEqual(recieved.query?.contains("after_id=\(image.id)"),true,"after_id query param")
    }
}
