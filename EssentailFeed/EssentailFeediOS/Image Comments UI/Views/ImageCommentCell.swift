//
//  ImageCommentCell.swift
//  EssentailFeediOS
//
//  Created by MIF50 on 03/02/2022.
//

import UIKit

public class ImageCommentCell: UITableViewCell {
    @IBOutlet private(set) public var usernameLabel: UILabel!
    @IBOutlet private(set) public var messageLabel: UILabel!
    @IBOutlet private(set) public var dateLabel: UILabel!
}
