//
//  UIRefreshController+Helpers.swift
//  EssentailFeediOS
//
//  Created by MIF50 on 09/12/2021.
//

import UIKit

extension UIRefreshControl {
    
    func update(isRefreshing: Bool) {
        isRefreshing ? beginRefreshing() : endRefreshing()
    }
}
