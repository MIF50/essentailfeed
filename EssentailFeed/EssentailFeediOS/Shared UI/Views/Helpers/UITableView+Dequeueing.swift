//
//  UITableView+Dequeueing.swift
//  EssentailFeediOS
//
//  Created by MIF50 on 10/11/2021.
//

import UIKit

extension UITableView {
    
    func dequeueReusableCell<T: UITableViewCell>()-> T {
        let identifier = String(describing: T.self)
        let cell = dequeueReusableCell(withIdentifier: identifier) as! T
        return cell
    }
}
