//
//  LoadMoreCellController.swift
//  EssentailFeediOS
//
//  Created by Mohamed Ibrahim on 29/03/2022.
//

import UIKit
import EssentailFeed

public class LoadMoreCellController: NSObject, UITableViewDataSource, UITableViewDelegate {
    
    private let cell = LoadMoreCell()
    private let callback: (()-> Void)
    private var offsetObserver: NSKeyValueObservation?
    
    public init(callback: @escaping (()-> Void)) {
        self.callback = callback
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        1
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        cell.selectionStyle = .none
        return cell
    }
    
    public func tableView(_ tableView: UITableView, willDisplay: UITableViewCell, forRowAt indexPath: IndexPath) {
        loadMoreIfNeeded()
        offsetObserver = tableView.observe(\.contentOffset, options: .new,changeHandler: { [weak self] tableView, _ in
            guard tableView.isDragging else { return }
            self?.loadMoreIfNeeded()
        })
    }
    
    public func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        offsetObserver = nil
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        loadMoreIfNeeded()
    }
    
    private func loadMoreIfNeeded() {
        guard !cell.isLoading else { return }
        
        callback()
    }
}

extension LoadMoreCellController: ResourceLoadingView, ResourceErrorView {
    
    public func display(_ viewModel: ResourceLoadingViewModel) {
        cell.isLoading = viewModel.isLoading
    }
    
    public func display(_ viewModel: ResourceErrorViewModel) {
        cell.message = viewModel.message
    }
}
