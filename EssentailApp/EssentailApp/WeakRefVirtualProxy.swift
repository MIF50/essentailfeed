//
//  WeakRefVirtualProxy.swift
//  EssentailFeediOS
//
//  Created by MIF50 on 12/11/2021.
//

import UIKit
import EssentailFeed

final class WeakRefVirtualProxy<T: AnyObject> {
    weak var object: T?
    
    init(_ object:T){
        self.object = object
    }
}

extension WeakRefVirtualProxy: ResourceLoadingView where T: ResourceLoadingView {
    
    func display(_ viewModel: ResourceLoadingViewModel) {
        object?.display(viewModel)
    }
}

extension WeakRefVirtualProxy: ResourceView where T: ResourceView,T.ResourceViewModel == UIImage {
    
    func display(_ model: UIImage) {
        object?.display(model)
    }
}

extension WeakRefVirtualProxy: ResourceErrorView where T: ResourceErrorView  {
    
    func display(_ viewModel: ResourceErrorViewModel) {
        object?.display(viewModel)
    }
}
