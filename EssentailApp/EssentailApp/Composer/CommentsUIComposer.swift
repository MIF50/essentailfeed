//
//  CommentsUIComposer.swift
//  EssentailApp
//
//  Created by MIF50 on 12/02/2022.
//

import UIKit
import EssentailFeed
import EssentailFeediOS
import Combine

public final class CommentsUIComposer {
    
    private init() {}
    
    public static func commentsComposeWith(commentsLoader: @escaping ()->  AnyPublisher<[ImageComment],Error>)-> ListViewController {
        
        let presentationAdapter = LoadResourcePresentationAdapter<[ImageComment],CommentsViewAdapter>(loader: { commentsLoader() })
        
        let commentsViewController = ListViewController.makeWith(title: ImageCommentsPresenter.title)
        commentsViewController.onRefresh = presentationAdapter.loadResource
        
        let commentsViewAdapter = CommentsViewAdapter(controller: commentsViewController)
        presentationAdapter.presenter = LoadResourcePresenter(resourceView: commentsViewAdapter,
                                                              loadingView: WeakRefVirtualProxy(commentsViewController),
                                                              errorView: WeakRefVirtualProxy(commentsViewController),
                                                              mapper: { ImageCommentsPresenter.map($0) })
        return commentsViewController
    }
}

private extension ListViewController {
    
    static func makeWith(title: String)-> ListViewController {
        let bundle = Bundle(for: ListViewController.self)
        let storyboard = UIStoryboard(name: "ImageComments", bundle: bundle)
        let controller = storyboard.instantiateInitialViewController() as! ListViewController
        
        controller.title = title
        
        return controller
    }
}

private class CommentsViewAdapter: ResourceView {
    weak var controller: ListViewController?
    
    init(controller: ListViewController) {
        self.controller = controller
    }
    
    func display(_ viewModel: ImageCommentsViewModel) {
        controller?.display(viewModel.comments.map { viewModel in
            CellController(id: viewModel, ImageCommentCellController(model: viewModel))
        })
    }
}
