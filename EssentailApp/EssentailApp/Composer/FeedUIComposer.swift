//
//  FeedUIComposer.swift
//  EssentailFeediOS
//
//  Created by MIF50 on 21/10/2021.
//

import UIKit
import EssentailFeed
import EssentailFeediOS
import Combine

public final class FeedUIComposer {
    
    private init() {}
    
    private typealias FeedPresentationAdapter = LoadResourcePresentationAdapter<Paginated<FeedImage>,FeedViewAdapter>
    
    public static func feedComposeWith(
        feedLoader: @escaping ()->  AnyPublisher<Paginated<FeedImage>,Error>,
        imageLoader: @escaping (URL)-> FeedImageDataLoader.Publisher,
        selection: @escaping ((FeedImage)-> Void) = { _ in }
    )-> ListViewController {
        
        let presentationAdapter = FeedPresentationAdapter(loader: { feedLoader().dispatchOnMainQueue() })
        
        let feedViewController = ListViewController.makeWith(title: FeedPresenter.title)
        feedViewController.onRefresh = presentationAdapter.loadResource
        
        let feedViewAdapter = FeedViewAdapter(
            controller: feedViewController,
            imageLoader: { imageLoader($0) },
            selection: selection
        )
        
        presentationAdapter.presenter = LoadResourcePresenter(resourceView: feedViewAdapter,
                                                              loadingView: WeakRefVirtualProxy(feedViewController),
                                                              errorView: WeakRefVirtualProxy(feedViewController),
                                                              mapper: { $0 })
        return feedViewController
    }
}

private extension ListViewController {
    
    static func makeWith(title: String)-> ListViewController {
        let bundle = Bundle(for: ListViewController.self)
        let storyboard = UIStoryboard(name: "Feed", bundle: bundle)
        let feedViewController = storyboard.instantiateInitialViewController() as! ListViewController
        
        feedViewController.title = title
        
        return feedViewController
    }
}
