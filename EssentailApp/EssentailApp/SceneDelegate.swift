//
//  SceneDelegate.swift
//  EssentailApp
//
//  Created by MIF50 on 21/12/2021.
//

import os
import UIKit
import EssentailFeed
import CoreData
import EssentailFeediOS
import CoreData
import Combine

class SceneDelegate: UIResponder, UIWindowSceneDelegate {
    
    var window: UIWindow?
    
    private lazy var logger = Logger(subsystem: "main", category: "main")
    
    private lazy var schedule = DispatchQueue(
        label: "com.essentailDeveloper.com.infra.queue",
        qos: .userInitiated,
        attributes: .concurrent
    ).eraseToAnyScheduler()
    
    private lazy var navigationController: UINavigationController = UINavigationController(rootViewController:
                                                                                            FeedUIComposer.feedComposeWith(
                                                                                                feedLoader: makeRemoteFeedLoaderWithFallback,
                                                                                                imageLoader: makeLocalFeedImageDataLoaderWithRemoteFallback,
                                                                                                selection: showComments))
    
    private let baseURL: URL = URL(string: "https://ile-api.essentialdeveloper.com/essential-feed")!
    
    private lazy var httpClient: HTTPClient = {
        URLSessionHTTPClient(session: URLSession(configuration: .ephemeral))
    }()
    
    private lazy var log = Logger(subsystem: "com.mif50.essentialFeed", category: "main")
    
    private lazy var store: FeedStore & FeedImageDataStore = {
        do {
            return try CoreDataFeedStore(
                storeURL: NSPersistentContainer
                    .defaultDirectoryURL()
                    .appendingPathComponent("feed-store.sqlite")
            )
        } catch {
            assertionFailure("Failed to instantiate CoreDataSotre with error \(error.localizedDescription)")
            log.fault("Failed to instantiate CoreDataStore with error \(error.localizedDescription)")
            return NullStore()
        }
        
    }()
    
    private lazy var localFeedLoader: LocalFeedLoader = {
        LocalFeedLoader(store: store, currentDate: Date.init)
    }()
    
    convenience init(httpClient: HTTPClient, store: FeedStore & FeedImageDataStore,scheduler: AnyDispatchQueueScheduler) {
        self.init()
        self.httpClient = httpClient
        self.store = store
        self.schedule = scheduler
    }
    
    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        guard let scene = (scene as? UIWindowScene) else { return }
        window = UIWindow(windowScene: scene)
        
        configureWindow()
    }
    
    func configureWindow() {
        window?.rootViewController = navigationController
        window?.makeKeyAndVisible()
    }
    
    private func showComments(for image: FeedImage) {
        let url = ImageCommentEndpoint.get(image.id).url(baseURL: baseURL)
        let comments = CommentsUIComposer.commentsComposeWith(commentsLoader: makeRemoteCommentsLoader(url: url))
        
        navigationController.pushViewController(comments, animated: true)
    }
    
    private func makeRemoteCommentsLoader(url: URL)-> ()->AnyPublisher<[ImageComment],Error> {
        return  { [httpClient] in
            httpClient
                .getPublisher(from: url)
                .tryMap(ImageCommentsMapper.map)
                .eraseToAnyPublisher()
        }
    }
    
    func sceneWillResignActive(_ scene: UIScene) {
        do {
            try localFeedLoader.validateCache()
        } catch {
            logger.error("Failed to validate cache with error: \(error.localizedDescription)")
        }
    }
    
    private func makeRemoteFeedLoaderWithFallback()-> AnyPublisher<Paginated<FeedImage>,Error> {
        makeRemoteFeedLoader()
            .caching(to: localFeedLoader)
            .fallback(to: localFeedLoader.loadPublisher)
            .map(makeFirstPage)
            .subscribe(on: schedule)
            .eraseToAnyPublisher()
    }
    
    private func makeRemoteLoadMoreLoader(last :FeedImage?) -> AnyPublisher<Paginated<FeedImage>, Error> {
        localFeedLoader.loadPublisher()
            .zip(makeRemoteFeedLoader(after: last))
            .map { cachedItems, newItems in
                return (cachedItems + newItems,newItems.last)
            }
            .map(makePage)
            .caching(to: localFeedLoader)
            .subscribe(on: schedule)
            .eraseToAnyPublisher()
    }
    
    private func makeRemoteFeedLoader(after: FeedImage? = nil)-> AnyPublisher<[FeedImage],Error> {
        let url = FeedEndpoint.get(after: after).url(baseURL: baseURL)
        return httpClient
            .getPublisher(from: url)
            .tryMap(FeedItemsMapper.map)
            .eraseToAnyPublisher()
    }
    
    private func makePage(_ items: [FeedImage],last: FeedImage?) -> Paginated<FeedImage> {
        Paginated(items: items,loadMorePublisher: last.map { last in
            { self.makeRemoteLoadMoreLoader(last: last) }
        })
    }
    
    private func makeFirstPage(_ items: [FeedImage]) -> Paginated<FeedImage> {
        makePage(items, last: items.last)
    }
    
    private func makeLocalFeedImageDataLoaderWithRemoteFallback(url: URL)-> FeedImageDataLoader.Publisher {
        let remoteImageLoader = httpClient.getPublisher(from: url).tryMap(FeedImageDataMapper.map)
        let localImageLoader = LocalFeedImageDataLoader(store: store)
        
        return localImageLoader
            .loadImageDataPublisher(from: url)
            .fallback { [schedule] in
                remoteImageLoader
                    .caching(to: localImageLoader, using: url)
                    .subscribe(on: schedule)
                    .eraseToAnyPublisher()
            }
            .subscribe(on: schedule)
            .eraseToAnyPublisher()
    }
}
