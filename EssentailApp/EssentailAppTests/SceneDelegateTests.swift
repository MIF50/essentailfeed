//
//  SceneDelegateTests.swift
//  EssentailAppTests
//
//  Created by MIF50 on 28/12/2021.
//

import XCTest
import EssentailFeediOS
@testable import EssentailApp

class SceneDelegateTests: XCTestCase {
    
    func test_configureWindow_setsWindowAsKeyAndVisible() {
        let window = WindowSpy()
        let sut = SceneDelegate()
        sut.window = window
        
        sut.configureWindow()
        
        XCTAssertEqual(window.makeKeyAndVisibleCallCount, 1, "Expected to make window key and visible")
    }
    
    
    func test_configureWindow_configuresRootViewController() {
        let sut = SceneDelegate()
        sut.window = UIWindow()
        
        sut.configureWindow()
        
        let root = sut.window?.rootViewController
        let rootNavigation = root as? UINavigationController
        let topController = rootNavigation?.topViewController
        
        XCTAssertNotNil(rootNavigation,"Expected a navigation controller as root, got\(String(describing: root)) instead")
        XCTAssertTrue(topController is ListViewController , "Expected a feed controller as top view controller,got \(String(describing: topController)) instead")
    }
}

private class WindowSpy: UIWindow {
    
    var makeKeyAndVisibleCallCount = 0
    
    override func makeKeyAndVisible() {
        makeKeyAndVisibleCallCount += 1
    }
}
