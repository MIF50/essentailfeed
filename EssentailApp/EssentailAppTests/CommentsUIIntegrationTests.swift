//
//  CommentsUIIntegrationTests.swift
//  EssentailAppTests
//
//  Created by MIF50 on 12/02/2022.
//

import XCTest
import Combine
import UIKit
import EssentailFeed
import EssentailFeediOS
import EssentailApp

class CommentsUIIntegrationTests: XCTestCase {
    
    func test_commentsView_hasTitle() {
        let (sut,_) = makeSUT()
        
        sut.loadViewIfNeeded()
        
        XCTAssertEqual(sut.title, commentsTitle)
    }
    
    func test_loadCommentsActions_requestCommentsFromLoader() {
        let (sut,loader) = makeSUT()
        
        XCTAssertEqual(loader.commentsCallCount, 0,"Expect no loading requests before view is loaded")

        sut.loadViewIfNeeded()
        XCTAssertEqual(loader.commentsCallCount, 1,"Expect a loading request once view is loaded")
        
        sut.simulateUserInitiatedReload()
        XCTAssertEqual(loader.commentsCallCount, 1,"Expect no requests until previous completes")

        loader.completeCommentsLoading(at: 0)
        sut.simulateUserInitiatedReload()
        XCTAssertEqual(loader.commentsCallCount, 2,"Expect another loading request once user initaites a load")

        loader.completeCommentsLoading(at: 1)
        sut.simulateUserInitiatedReload()
        XCTAssertEqual(loader.commentsCallCount, 3,"Expect a third loading request once user initiates anohter load")
    }
    
    func test_loadingCommentsIndicator_isVisibleWhileLoadingComments() {
        let (sut,loader) = makeSUT()
        
        sut.loadViewIfNeeded()
        XCTAssertTrue(sut.isShowingLoadingIndicator,"Expect loading indicator once view is loaded ")
        
        loader.completeCommentsLoading(at: 0)
        XCTAssertFalse(sut.isShowingLoadingIndicator,"Expect no loading indicator once loading is complete successfully")
        
        sut.simulateUserInitiatedReload()
        XCTAssertTrue(sut.isShowingLoadingIndicator,"Expect loading indicator once user initiates a reload")
        
        loader.completeCommentsLoadingWithError(at: 1)
        XCTAssertFalse(sut.isShowingLoadingIndicator, "Expect no loading indicator once user initaited a loading is complete with failure")
    }
    
    func test_loadCommentsCompletion_rendersSuccessfullyLoadedComments() {
        let comment0 = makeComment(message: "a message",username: "a username")
        let comment1 = makeComment(message: "anthor message",username: "anthor username")
        let (sut,loader) = makeSUT()
        
        sut.loadViewIfNeeded()
        assertThat(sut, isRendering: [ImageComment]())
        
        loader.completeCommentsLoading(with: [comment0],at: 0)
        assertThat(sut, isRendering: [comment0])
        
        sut.simulateUserInitiatedReload()
        loader.completeCommentsLoading(with: [comment0,comment1], at: 1)
        assertThat(sut, isRendering: [comment0,comment1])
    }
    
    func test_loadCommentsCompletions_rendersSuccessfullyLoadedEmptyFeedAfterNonEmpty() {
        let comment = makeComment()
        let (sut, loader) = makeSUT()
        
        sut.loadViewIfNeeded()
        loader.completeCommentsLoading(with: [comment], at: 0)
        assertThat(sut,isRendering: [comment])
        
        sut.simulateUserInitiatedReload()
        loader.completeCommentsLoading(with: [], at: 1)
        assertThat(sut, isRendering: [ImageComment]())
    }
    
    func test_loadCommentsCompletion_doesNotAlterCurrentRenderingStateOnError() {
        let comment = makeComment()
        let (sut,loader) = makeSUT()
        
        sut.loadViewIfNeeded()
        loader.completeCommentsLoading(with: [comment], at: 0)
        assertThat(sut, isRendering: [comment])
        
        sut.simulateUserInitiatedReload()
        loader.completeCommentsLoadingWithError(at: 1)
        assertThat(sut, isRendering: [comment])
    }
    
    func test_loadCommentsCompletion_rendersErrorMessageOnErrorUntilNextReload() {
        let (sut, loader) = makeSUT()
        
        sut.loadViewIfNeeded()
        XCTAssertEqual(sut.errorMessage, nil)
        
        loader.completeCommentsLoadingWithError(at: 0)
        XCTAssertEqual(sut.errorMessage, loadError)
        
        sut.simulateUserInitiatedReload()
        XCTAssertEqual(sut.errorMessage, nil)
    }
    
    func test_tapOnErrorView_HideErrorViewMessage() {
        let (sut, loader) = makeSUT()
        
        sut.loadViewIfNeeded()
        XCTAssertEqual(sut.errorMessage, nil)
        
        loader.completeCommentsLoadingWithError(at: 0)
        XCTAssertEqual(sut.errorMessage, loadError)
        
        sut.simulateErrorViewTap()
        XCTAssertEqual(sut.errorMessage, nil)
    }
    
    func test_loadCommentsCompleteion_dispatchesFromBackgroundToMainThread() {
        let (sut,loader) = makeSUT()
        sut.loadViewIfNeeded()
        
        let exp = expectation(description: "Waiting for background queue")
        DispatchQueue.global().async {
            loader.completeCommentsLoading(at: 0)
            exp.fulfill()
        }
        wait(for: [exp], timeout: 1.0)
    }
    
    func test_deinit_cancelsRunningRequest() {
        var cancelCallCount = 0
        
        var sut: ListViewController?
        autoreleasepool {
            sut = CommentsUIComposer.commentsComposeWith {
                PassthroughSubject<[ImageComment],Error>()
                    .handleEvents(receiveCancel: {
                        cancelCallCount += 1
                    })
                    .eraseToAnyPublisher()
            }
            
            sut?.loadViewIfNeeded()
        }
        
        XCTAssertEqual(cancelCallCount, 0)
        
        sut = nil
        
        XCTAssertEqual(cancelCallCount, 1)
    }
    
    // MARK: - Helper
    
    private func makeSUT(file: StaticString = #file, line: UInt = #line)-> (sut: ListViewController,loader: LoaderSpy) {
        let loader = LoaderSpy()
        let sut = CommentsUIComposer.commentsComposeWith(commentsLoader: loader.loadPublisher)
        trackForMemoryLeaks(loader, file: file, line: line)
        trackForMemoryLeaks(sut, file: file, line: line)
        return (sut,loader)
    }
    
    private func assertThat(_ sut: ListViewController,
                            isRendering comments: [ImageComment],
                            file: StaticString = #file,
                            line: UInt = #line) {
        
        guard sut.numberOfRenderedCommentsViews == comments.count else {
            return XCTFail("Expected \(comments.count) comments, got \(sut.numberOfRenderedFeedImageViews) instead.", file: file, line: line)
        }
        
        let viewModel = ImageCommentsPresenter.map(comments)
        
        viewModel.comments.enumerated().forEach { index,comment in
            XCTAssertEqual(sut.commentMessage(at: index), comment.message,"message at \(index)",file: file,line: line)
            XCTAssertEqual(sut.commentDate(at: index), comment.date,"date at \(index)",file: file,line: line)
            XCTAssertEqual(sut.commentUsername(at: index), comment.username,"username at \(index)",file: file,line: line)
        }
    }
    
    private func makeComment(message: String = "any message",
                             username: String = "any username")-> ImageComment {
        return ImageComment(id: UUID(), message: message, createdAt: Date(), username: username)
    }
    
    private class LoaderSpy {
        
        private(set) var requests = [PassthroughSubject<[ImageComment],Error>]()
        
        var commentsCallCount: Int {
            return requests.count
        }
                
        func loadPublisher()-> AnyPublisher<[ImageComment],Error> {
           let publisher = PassthroughSubject<[ImageComment],Error>()
            requests.append(publisher)
            return publisher.eraseToAnyPublisher()
        }
        
        func completeCommentsLoading(with comments:[ImageComment] = [],at index: Int = 0) {
            requests[index].send(comments)
            requests[index].send(completion: .finished)
        }
        
        func completeCommentsLoadingWithError(at index: Int){
            let error = NSError(domain: "test error", code: 0)
            requests[index].send(completion: .failure(error))
        }
    }
}
