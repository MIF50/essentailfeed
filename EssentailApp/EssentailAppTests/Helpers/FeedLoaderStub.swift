//
//  FeedLoaderStub.swift
//  EssentailAppTests
//
//  Created by MIF50 on 23/12/2021.
//

import EssentailFeed

class FeedLoaderStub {
    private let result: Swift.Result<[FeedImage],Error>
    
    init(result: Swift.Result<[FeedImage],Error>) {
        self.result = result
    }
    
    func load(completion: @escaping ((Swift.Result<[FeedImage],Error>) -> Void)) {
        completion(result)
    }
}
