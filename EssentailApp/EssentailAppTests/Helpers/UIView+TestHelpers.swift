//
//  UIView+TestHelpers.swift
//  EssentailAppTests
//
//  Created by MIF50 on 01/01/2022.
//

import UIKit

extension UIView {
    
    func enforceLayoutCycle() {
        layoutIfNeeded()
        RunLoop.current.run(until: Date())
    }
}
