//
//  SharedTestHelpers.swift
//  EssentailAppTests
//
//  Created by MIF50 on 22/12/2021.
//

import Foundation
import EssentailFeed

func anyNSError()-> NSError {
    return NSError(domain: "any error", code: 0)
}

func anyURL()-> URL {
    return URL(string: "https://any-url.com")!
}

func anyData()-> Data {
    Data("any data".utf8)
}

func uniqueFeed() -> [FeedImage] {
    [FeedImage(id: UUID(), description: "any description", location: "any location", url: anyURL())]
}

private class DummyView: ResourceView {
    func display(_ viewModel: Any) {}
}

var loadError: String {
    LoadResourcePresenter<Any,DummyView>.loadError
}

var feedTitle: String {
    FeedPresenter.title
}

var commentsTitle: String {
    ImageCommentsPresenter.title
}
