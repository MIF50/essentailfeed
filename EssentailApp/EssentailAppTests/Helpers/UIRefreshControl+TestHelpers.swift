//
//  UIRefreshControl+TestHelpers.swift
//  EssentailFeediOSTests
//
//  Created by MIF50 on 12/11/2021.
//

import UIKit

extension UIRefreshControl {
    func simulatePullToRefresh() {
        self.allTargets.forEach({ target in
            self.actions(forTarget: target, forControlEvent: .valueChanged)?.forEach({
                (target as NSObject).perform(Selector($0))
            })
        })
    }
}
