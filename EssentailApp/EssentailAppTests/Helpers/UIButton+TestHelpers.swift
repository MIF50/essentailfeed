//
//  UIButton+TestHelper.swift
//  EssentailFeediOSTests
//
//  Created by MIF50 on 12/11/2021.
//

import EssentailFeed
import UIKit

extension UIButton {
    func simulateTap() {
        allTargets.forEach { target in
            actions(forTarget: target, forControlEvent: .touchUpInside)?.forEach({
                (target as NSObject).perform(Selector($0))
            })
        }
    }
}
