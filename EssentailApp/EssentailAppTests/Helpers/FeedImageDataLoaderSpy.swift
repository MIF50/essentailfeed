//
//  FeedImageDataLoaderSpy.swift
//  EssentailAppTests
//
//  Created by MIF50 on 24/12/2021.
//

import EssentailFeed

//class FeedImageDataLoaderSpy: FeedImageDataLoader {
//    
//    private(set) var messages = [(url: URL,completion: ((FeedImageDataLoader.Result) -> Void))]()
//    
//    var loadedURLs: [URL] {
//        messages.map { $0.url }
//    }
//    
//    private(set) var cancelledURLs = [URL]()
//    
//    private struct TaskWrapper: FeedImageDataLoaderTask {
//        let callback: (()-> Void)
//        func cancel() {
//            callback()
//        }
//    }
//    
//    func loadImageData(from url: URL,
//                       completion: @escaping ((FeedImageDataLoader.Result) -> Void)) -> FeedImageDataLoaderTask {
//        messages.append((url,completion))
//        
//        return TaskWrapper { [weak self] in
//            self?.cancelledURLs.append(url)
//        }
//    }
//    
//    func complete(with data: Data,at index: Int = 0) {
//        messages[index].completion(.success(data))
//    }
//    
//    func complete(with error: NSError,at index: Int = 0) {
//        messages[index].completion(.failure(error))
//    }
//    
//}
