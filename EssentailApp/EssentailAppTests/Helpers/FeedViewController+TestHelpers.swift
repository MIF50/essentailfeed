//
//  FeedViewController+Helper.swift
//  EssentailFeediOSTests
//
//  Created by MIF50 on 12/11/2021.
//

import UIKit
import EssentailFeediOS

extension ListViewController {
    
    public override func loadViewIfNeeded() {
        super.loadViewIfNeeded()
        
        tableView.frame = .init(x: 0, y: 0, width: 1, height: 1)
    }
    
    func simulateUserInitiatedReload() {
        refreshControl?.simulatePullToRefresh()
    }
    
    var isShowingLoadingIndicator: Bool {
        return self.refreshControl?.isRefreshing == true
    }
    
    func simulateErrorViewTap() {
        errorView.simulateTap()
    }
    
    var errorMessage: String? {
        return errorView.message
    }
}

extension ListViewController {
    
    var numberOfRenderedCommentsViews: Int {
        return tableView.numberOfSections == 0 ? 0 : tableView.numberOfRows(inSection: commentsSection)
    }
    
    func commentMessage(at row: Int)-> String? {
        imageCommentView(at: row)?.messageLabel.text
    }
    
    func commentDate(at row: Int)-> String? {
        imageCommentView(at: row)?.dateLabel.text
    }
    
    func commentUsername(at row: Int)-> String? {
        imageCommentView(at: row)?.usernameLabel.text
    }
    
    private func imageCommentView(at row: Int)-> ImageCommentCell? {
        guard numberOfRenderedCommentsViews > row else {
            return nil
        }
        let ds = tableView.dataSource
        let indexPath = IndexPath(row: row, section: commentsSection)
        return ds?.tableView(tableView, cellForRowAt: indexPath) as? ImageCommentCell
    }
    
    private var commentsSection: Int {
        return 0
    }
}

extension ListViewController {
    
    var numberOfRenderedFeedImageViews: Int {
        return numberOfRows(in: feedImageSection)
    }
    
    var canLoadMoreFeed: Bool {
        loadMoreFeedCell() != nil
    }
    
    func feedImageView(at row: Int)-> UITableViewCell? {
        cell(at: row, section: feedImageSection)
    }
    
    func renderedFeedImageData(at index: Int)-> Data? {
        return simulateFeedImageViewVisible(at: index)?.renderedImage
    }
    
    @discardableResult
    func simulateFeedImageViewVisible(at index: Int)-> FeedImageCell? {
        return feedImageView(at: index) as? FeedImageCell
    }
    
    @discardableResult
    func simulateFeedImageViewNotVisible(at index:Int)-> FeedImageCell? {
        let view = simulateFeedImageViewVisible(at: index)
        
        let delegate = tableView.delegate
        let indexPath = IndexPath(row: index, section: feedImageSection)
        delegate?.tableView?(tableView, didEndDisplaying: view!, forRowAt: indexPath)
        return view
    }
    
    func simulateFeedImageViewNearVisible(at row: Int) {
        let ds = tableView.prefetchDataSource
        let indexPath = IndexPath(row: row, section: feedImageSection)
        ds?.tableView(tableView, prefetchRowsAt: [indexPath])
    }
    
    func simulateLoadMoreFeedAction() {
        guard let view = loadMoreFeedCell() else { return }
        
        let delegate = tableView.delegate
        let indexPath = IndexPath(row: 0, section: feedLoadMoreSection)
        delegate?.tableView?(tableView, willDisplay: view, forRowAt: indexPath)
    }
    
    func simulateTapOnLoadMoreError() {
        let delegate = tableView.delegate
        let indexPath = IndexPath(row: 0, section: feedLoadMoreSection)
        delegate?.tableView?(tableView, didSelectRowAt: indexPath)
    }
    
    var isShowingLoadingMoreFeedIndicator: Bool {
        loadMoreFeedCell()?.isLoading == true
    }
    
    var loadMoreFeedErrorMessage: String? {
        loadMoreFeedCell()?.message
    }
    
    private func loadMoreFeedCell()-> LoadMoreCell? {
        cell(at: 0, section: feedLoadMoreSection) as? LoadMoreCell
    }
    
    func simulateFeedImageViewNotNearVisible(at row: Int) {
        simulateFeedImageViewNearVisible(at: row)
        
        let ds = tableView.prefetchDataSource
        let indexPath = IndexPath(row: row, section: feedImageSection)
        ds?.tableView?(tableView, cancelPrefetchingForRowsAt: [indexPath])
    }
    
    func simulateTapOnFeedImage(at row: Int) {
        let delegate = tableView.delegate
        let indexPath = IndexPath(row: row, section: feedImageSection)
        delegate?.tableView?(tableView, didSelectRowAt: indexPath)
    }
    
    func cell(at row: Int,section: Int)-> UITableViewCell? {
        guard numberOfRows(in: section) > row else {
            return nil
        }
        let ds = tableView.dataSource
        let indexPath = IndexPath(row: row, section: section)
        return ds?.tableView(tableView, cellForRowAt: indexPath)
    }
    
    func numberOfRows(in section: Int)-> Int {
        tableView.numberOfSections > section ? tableView.numberOfRows(inSection: section) : 0
    }
    
    private var feedImageSection: Int { 0 }
    
    private var feedLoadMoreSection: Int { 1 }
}
