//
//  XCTestCase+MemoryLeakTracking.swift
//  EssentailAppTests
//
//  Created by MIF50 on 22/12/2021.
//

import XCTest

extension XCTestCase {
    
    func trackForMemoryLeaks(_ instance: AnyObject,
                                     file: StaticString = #file,
                                     line: UInt = #line) {
        addTeardownBlock { [weak instance] in
            XCTAssertNil(instance,"Instance should be have been deallocated. Potential memory leak.",file: file,line: line)
        }
    }
}
