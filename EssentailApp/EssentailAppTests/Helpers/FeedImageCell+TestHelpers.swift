//
//  FeedImageCell+Helper.swift
//  EssentailFeediOSTests
//
//  Created by MIF50 on 12/11/2021.
//

import XCTest
import EssentailFeediOS

extension FeedImageCell {
    
    var isShowingLocation: Bool {
        return !self.locationContainer.isHidden
    }
    
    var isShowingImageLoadingIndicator: Bool {
        return feedImageContainer.isShimmering
    }
    
    var locationText: String? {
        return self.locationLabel.text
    }
    
    var descriptionText: String? {
        return self.descriptionLabel.text
    }
    
    var renderedImage:Data? {
        return self.feedImageView.image?.pngData()
    }
    
    var isShowingRetryAction: Bool {
        return !self.feedImageRetryButton.isHidden
    }
    
    func simulateRetryAction() {
        feedImageRetryButton.simulateTap()
    }
}

